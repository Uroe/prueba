/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.unaplanilla.model;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Navarro
 */
public class BancosDto {
    
    public SimpleStringProperty id;
    public SimpleBooleanProperty estado;
    public SimpleStringProperty nombre;
    public ObjectProperty<String> Acomicion;
    public SimpleStringProperty comicionTransferencia;

    public BancosDto() {
        this.id = new SimpleStringProperty();
        this.estado = new SimpleBooleanProperty(true);
        this.nombre = new SimpleStringProperty();
        this.Acomicion = new SimpleObjectProperty("E");
        this.comicionTransferencia = new SimpleStringProperty();
    }

    public Long getId() {
        if (id.get() != null && !id.get().isEmpty()) {
            return Long.valueOf(id.get());
        } else {
            return null;
        }
    }

    public void setId(Long id) {
        this.id.set(id.toString());
    }

    public String getEstado() {
        return estado.getValue() ? "A" : "I";
    }

    public void setEstado(String estado) {
        this.estado.setValue(estado.equalsIgnoreCase("A"));
    }

    public String getNombre() {
        return nombre.get();
    }

    public void setNombre(String nombre) {
        this.nombre.set(nombre);
    }

    public String getAcomicion() {
        return Acomicion.get();
    }

    public void setAcomicion(String Acomicion) {
        this.Acomicion.set(Acomicion);
    }

    public String getComicionTransferencia() {
        return comicionTransferencia.get();
    }

    public void setComicionTransferencia(String comicionTransferencia) {
        this.comicionTransferencia.set(comicionTransferencia);
    }

    @Override
    public String toString() {
        return "BancosDto{" + "id=" + id + ", estado=" + estado + ", nombre=" + nombre + ", Acomicion=" + Acomicion + ", comicionTransferencia=" + comicionTransferencia + '}';
    }
}
