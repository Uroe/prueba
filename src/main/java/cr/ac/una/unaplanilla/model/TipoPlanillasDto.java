/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.unaplanilla.model;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Navarro
 */
public class TipoPlanillasDto {

    public SimpleStringProperty id;
    public SimpleStringProperty codigo;
    public SimpleStringProperty descripcion;
    public SimpleStringProperty planillaMes;
    public SimpleBooleanProperty estado;
    public SimpleStringProperty idEmpleado;
    public SimpleStringProperty nombre;

    public TipoPlanillasDto() {
        this.id = new SimpleStringProperty();
        this.codigo = new SimpleStringProperty();
        this.descripcion = new SimpleStringProperty();
        this.planillaMes = new SimpleStringProperty();
        this.estado = new SimpleBooleanProperty(true);
        this.idEmpleado = new SimpleStringProperty();
        this.nombre = new SimpleStringProperty();
    }

    public Long getId() {
        if (id.get() != null && !id.get().isEmpty()) {
            return Long.valueOf(id.get());
        } else {
            return null;
        }
    }

    public void setId(Long id) {
        this.id.set(id.toString());
    }

    public String getCodigo() {
        return codigo.get();
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    public String getDescripcion() {
        return descripcion.get();
    }

    public void setDescripcion(String descripcion) {
        this.descripcion.set(descripcion);
    }

    public String getPlanillaMes() {
        return planillaMes.get();
    }

    public void setPlanillaMes(String planillaMes) {
        this.planillaMes.set(planillaMes);
    }

    public String getEstado() {
        return estado.getValue() ? "A" : "I";
    }

    public void setEstado(String estado) {
        this.estado.setValue(estado.equalsIgnoreCase("A"));
    }

    public Long getIdEmpleado() {
        if (idEmpleado.get() != null && !idEmpleado.get().isEmpty()) {
            return Long.valueOf(idEmpleado.get());
        } else {
            return null;
        }
    }

    public void setIdEmpleado(Long idEmpleado) {
        this.idEmpleado.set(idEmpleado.toString());
    }

    public String getNombre() {
        return nombre.get();
    }

    public void setNombre(String nombre) {
        this.nombre.set(nombre);
    }
    
    @Override
    public String toString() {
        return "PlanillaDto{" + "id=" + id + ", codigo=" + codigo + ", descripcion=" + descripcion + ", planillaMes=" + planillaMes + ", estado=" + estado + ", idEmpleado=" + idEmpleado + ", nombre=" + nombre + '}';
    }

}
