/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.unaplanilla.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import cr.ac.una.unaplanilla.model.TipoPlanillasDto;
import cr.ac.una.unaplanilla.service.TipoPlanillasService;
import cr.ac.una.unaplanilla.util.Formato;
import cr.ac.una.unaplanilla.util.Mensaje;
import cr.ac.una.unaplanilla.util.Respuesta;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author JosueNG
 */
public class TipoPlanillasViewController extends Controller implements Initializable {

    @FXML
    private AnchorPane root;
    @FXML
    private JFXTextField txtPlanillaMes;
    @FXML
    private ChoiceBox<?> choicePlanilla;
    @FXML
    private TableView<?> tableDato;
    @FXML
    private JFXTextField txtId;
    @FXML
    private JFXTextField txtCodigo;
    @FXML
    private JFXTextField txtDescripcion;
    @FXML
    private JFXTextField txtIdEmpleado;
    @FXML
    private JFXTextField txtNombre;
    @FXML
    private JFXButton btnNuevo;
    @FXML
    private JFXButton btnBuscar;
    @FXML
    private JFXButton btnEliminar;
    @FXML
    private JFXButton btnGuardar;
    @FXML
    private JFXCheckBox chkActivo;

    TipoPlanillasDto planilla;
    List<Node> requeridos = new ArrayList<>();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        txtCodigo.setTextFormatter(Formato.getInstance().cedulaFormat(15));
        txtDescripcion.setTextFormatter(Formato.getInstance().maxLengthFormat(50));
        txtPlanillaMes.setTextFormatter(Formato.getInstance().cedulaFormat(5));
        txtNombre.setTextFormatter(Formato.getInstance().letrasFormat(20));
        planilla = new TipoPlanillasDto();
        nuevoPlanilla();
        indicarRequeridos();
    }

    private void bindPlanilla(Boolean nuevo) {
        if (!nuevo) {
            txtId.textProperty().bind(planilla.id);
        }
        txtCodigo.textProperty().bindBidirectional(planilla.codigo);
        txtDescripcion.textProperty().bindBidirectional(planilla.descripcion);
        txtPlanillaMes.textProperty().bindBidirectional(planilla.planillaMes);
        chkActivo.selectedProperty().bindBidirectional(planilla.estado);
        txtIdEmpleado.textProperty().bindBidirectional(planilla.idEmpleado);
        txtNombre.textProperty().bindBidirectional(planilla.nombre);
    }

    private void unbindPlanilla() {
        txtId.textProperty().unbind();
        txtCodigo.textProperty().unbindBidirectional(planilla.codigo);
        txtDescripcion.textProperty().unbindBidirectional(planilla.descripcion);
        txtPlanillaMes.textProperty().unbindBidirectional(planilla.planillaMes);
        chkActivo.selectedProperty().unbindBidirectional(planilla.estado);
        txtIdEmpleado.textProperty().unbindBidirectional(planilla.idEmpleado);
        txtNombre.textProperty().unbindBidirectional(planilla.nombre);
    }

    private void nuevoPlanilla() {
        unbindPlanilla();
        planilla = new TipoPlanillasDto();
        bindPlanilla(true);
        txtId.clear();
        txtId.requestFocus();
    }

    public void indicarRequeridos() {
        requeridos.clear();
        requeridos.addAll(Arrays.asList(txtCodigo, txtDescripcion, txtPlanillaMes));
    }

    public String validarRequeridos() {
        Boolean validos = true;
        String invalidos = " ";
        for (Node node : requeridos) {
            if (node instanceof JFXTextField && ((JFXTextField) node).getText() == null) {
                if (validos) {
                    invalidos += ((JFXTextField) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXTextField) node).getPromptText();
                }
                validos = false;
            } else if (node instanceof JFXPasswordField && ((JFXPasswordField) node).getText() == null) {
                if (validos) {
                    invalidos += ((JFXPasswordField) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXPasswordField) node).getPromptText();
                }
                validos = false;
            } else if (node instanceof JFXDatePicker && ((JFXDatePicker) node).getValue() == null) {
                if (validos) {
                    invalidos += ((JFXDatePicker) node).getAccessibleText();
                } else {
                    invalidos += "," + ((JFXDatePicker) node).getAccessibleText();
                }
                validos = false;
            } else if (node instanceof JFXComboBox && ((JFXComboBox) node).getSelectionModel().getSelectedIndex() < 0) {
                if (validos) {
                    invalidos += ((JFXComboBox) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXComboBox) node).getPromptText();
                }
                validos = false;
            }
        }
        if (validos) {
            return "";
        } else {
            return "Campos requeridos o con problemas de formato (" + invalidos + ").";
        }
    }

    @FXML
    private void onKeyPressedTxtId(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER && !txtId.getText().isEmpty()) {
            cargarPlanilla(Long.valueOf(txtId.getText()));
        }
    }

    @FXML
    private void onKeyPressedTxtIdEmpleado(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER && !txtIdEmpleado.getText().isEmpty()) {
            cargarEmpleado(Long.valueOf(txtIdEmpleado.getText()));
        }
    }

    @FXML
    private void onActionBtnNuevo(ActionEvent event) {
        if (new Mensaje().showConfirmation("Limpiar Planilla", getStage(), "Esta seguro que desea limpiar el registro?")) {
            nuevoPlanilla();
        }
    }

    @FXML
    private void onActionBtnBuscar(ActionEvent event) {
    }

    @FXML
    private void onActionBtnEliminar(ActionEvent event) {
        try {
            if (planilla.getId() == null) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Eliminar planilla", getStage(), "Dede cargar la planilla que desea eliminar");
            } else {
                TipoPlanillasService service = new TipoPlanillasService();
                Respuesta respuesta = service.eliminarPlanilla(planilla.getId());
                if (!respuesta.getEstado()) {
                    new Mensaje().showModal(Alert.AlertType.ERROR, "Eliminar planilla", getStage(), respuesta.getMensaje());
                } else {
                    new Mensaje().showModal(Alert.AlertType.INFORMATION, "Eliminar planilla", getStage(), "Planilla eliminado correctamente");
                    nuevoPlanilla();
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(TipoPlanillasViewController.class.getName()).log(Level.SEVERE, "Error eliminando la planilla.", ex);
            new Mensaje().showModal(Alert.AlertType.ERROR, "Eliminar planilla", getStage(), "Ocurrio un error eliminando la planilla");
        }
    }

    @FXML
    private void onActionBtnGuardar(ActionEvent event) {
        try {
            String invalidos = validarRequeridos();
            if (!invalidos.isEmpty()) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar planilla", getStage(), invalidos);
            } else {
                TipoPlanillasService service = new TipoPlanillasService();
                Respuesta respuesta = service.guardarPlanilla(planilla);
                if (!respuesta.getEstado()) {
                    new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar planilla", getStage(), respuesta.getMensaje());
                } else {
                    unbindPlanilla();
                    planilla = (TipoPlanillasDto) respuesta.getResultado("Planilla");
                    bindPlanilla(false);
                    new Mensaje().showModal(Alert.AlertType.INFORMATION, "Guardar planilla", getStage(), "Planilla guardado correctamente");
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(TipoPlanillasViewController.class.getName()).log(Level.SEVERE, "Error guardando la planilla.", ex);
            new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar planilla", getStage(), "Ocurrio un error guardando la planilla");
        }
    }

    private void cargarPlanilla(Long id) {
        TipoPlanillasService service = new TipoPlanillasService();
        Respuesta respuesta = service.getPlanilla(id);

        if (respuesta.getEstado()) {
            unbindPlanilla();
            planilla = (TipoPlanillasDto) respuesta.getResultado("Planilla");
            bindPlanilla(false);
            validarRequeridos();
        } else {
            new Mensaje().showModal(Alert.AlertType.ERROR, "Cargar Planilla", getStage(), respuesta.getMensaje());
        }
    }

    private void cargarEmpleado(Long idEmpleado) {
        TipoPlanillasService service = new TipoPlanillasService();
        Respuesta respuesta = service.getEmpleado(idEmpleado);

        if (respuesta.getEstado()) {
            unbindPlanilla();
            planilla = (TipoPlanillasDto) respuesta.getResultado("Empleado");
            bindPlanilla(false);
            validarRequeridos();
        } else {
            new Mensaje().showModal(Alert.AlertType.ERROR, "Cargar Empleado", getStage(), respuesta.getMensaje());
        }
    }

    @Override
    public void initialize() {
    }
}
