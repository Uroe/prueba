/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.unaplanilla.controller;


import animatefx.animation.Flash;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import cr.ac.una.unaplanilla.util.FlowController;
import cr.ac.una.unaplanilla.util.Mensaje;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Navarro
 */
public class LogInViewController extends Controller implements Initializable {

    @FXML
    private AnchorPane root;
    @FXML
    private ImageView imvFondo;
    @FXML
    private JFXTextField txtUsuario;
    @FXML
    private JFXTextField txtContrasena;
    @FXML
    private JFXButton btnCancelar;
    @FXML
    private JFXButton btnIngresar;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        imvFondo.fitHeightProperty().bind(root.heightProperty());
        imvFondo.fitWidthProperty().bind(root.widthProperty());
 
    }    

    @Override
    public void initialize() {
        
 
    }

    @FXML
    private void onActionBtnIngresar(ActionEvent event) {
        try {
            if (txtUsuario.getText() == null || txtUsuario.getText().isEmpty()) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Validacion de Usuario",
                        (Stage) btnIngresar.getScene().getWindow(),
                        "Es necesario digitar un usuario para ingresar al sistema!!");
                new Flash(txtUsuario).play();
            } else if (txtContrasena.getText() == null || txtContrasena.getText().isEmpty()) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Validacion de Contrasena",
                        (Stage) btnIngresar.getScene().getWindow(),
                        "Es necesario digitar un contrasena para ingresar al sistema!!");
                new Flash(txtContrasena).play();
            } else {
                FlowController.getInstance().goMain();
                ((Stage) btnIngresar.getScene().getWindow()).close();
            }
        } 
        catch (Exception ex){
            Logger.getLogger(LogInViewController.class.getName()).
                    log(Level.SEVERE,"Error Ingresando.",ex);
        }
    }

    @FXML
    private void salir(ActionEvent event) {
        ((Stage) btnIngresar.getScene().getWindow()).close();
    }
}

