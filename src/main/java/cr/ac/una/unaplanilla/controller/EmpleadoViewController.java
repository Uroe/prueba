/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.unaplanilla.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTextField;
import cr.ac.una.unaplanilla.model.EmpleadoDto;
import cr.ac.una.unaplanilla.service.EmpleadoService;
import cr.ac.una.unaplanilla.util.AppContext;
import cr.ac.una.unaplanilla.util.BindingUtils;
import cr.ac.una.unaplanilla.util.Formato;
import cr.ac.una.unaplanilla.util.Mensaje;
import cr.ac.una.unaplanilla.util.Respuesta;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Navarro
 */
public class EmpleadoViewController extends Controller implements Initializable {

    @FXML
    private AnchorPane root;
    @FXML
    private JFXTextField txtId;
    @FXML
    private JFXTextField txtNombre;
    @FXML
    private JFXTextField txtCedula;
    @FXML
    private JFXTextField txtCorreo;
    @FXML
    private JFXTextField txtUsuario;
    @FXML
    private JFXTextField txtContrasena;
    @FXML
    private JFXButton btnNuevo;
    @FXML
    private JFXButton btnBuscar;
    @FXML
    private JFXButton btnEliminar;
    @FXML
    private JFXButton btnGuardar;
    @FXML
    private JFXRadioButton rdbMasculino;
    @FXML
    private ToggleGroup tggGenero;
    @FXML
    private JFXRadioButton rdbFemenino;
    @FXML
    private JFXCheckBox chkAdministrador;
    @FXML
    private JFXCheckBox chkActivo;
    @FXML
    private JFXDatePicker dtpFIngreso;
    @FXML
    private JFXDatePicker dtpFSalida;
    @FXML
    private JFXTextField txtPApellido;
    @FXML
    private JFXTextField txtSApellido;

    EmpleadoDto empleado;
    List<Node> requeridos = new ArrayList<>();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        rdbMasculino.setUserData("M");
        rdbFemenino.setUserData("F");
//        txtId.setTextFormatter(Formato.getInstance().letrasFormat());
        txtNombre.setTextFormatter(Formato.getInstance().letrasFormat(30));
        txtPApellido.setTextFormatter(Formato.getInstance().letrasFormat(15));
        txtSApellido.setTextFormatter(Formato.getInstance().letrasFormat(15));
        txtCedula.setTextFormatter(Formato.getInstance().cedulaFormat(40));
        txtCorreo.setTextFormatter(Formato.getInstance().maxLengthFormat(80));
//        txtUsuario.setTextFormatter(Formato.getInstance().letrasFormat(15));
        txtUsuario.setTextFormatter(Formato.getInstance().cedulaFormat(15));
        txtContrasena.setTextFormatter(Formato.getInstance().maxLengthFormat(10));
        empleado = new EmpleadoDto();
        nuevoEmpleado();
        indicarRequeridos();
    }

    public void indicarRequeridos() {
        requeridos.clear();
        requeridos.addAll(Arrays.asList(txtNombre, txtPApellido, txtCedula, dtpFIngreso));
    }

    public String validarRequeridos() {
        Boolean validos = true;
        String invalidos = " ";
        for (Node node : requeridos) {
            if (node instanceof JFXTextField && ((JFXTextField) node).getText() == null) {
                if (validos) {
                    invalidos += ((JFXTextField) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXTextField) node).getPromptText();
                }
                validos = false;
            } else if (node instanceof JFXPasswordField && ((JFXPasswordField) node).getText() == null) {
                if (validos) {
                    invalidos += ((JFXPasswordField) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXPasswordField) node).getPromptText();
                }
                validos = false;
            } else if (node instanceof JFXDatePicker && ((JFXDatePicker) node).getValue() == null) {
                if (validos) {
                    invalidos += ((JFXDatePicker) node).getAccessibleText();
                } else {
                    invalidos += "," + ((JFXDatePicker) node).getAccessibleText();
                }
                validos = false;
            } else if (node instanceof JFXComboBox && ((JFXComboBox) node).getSelectionModel().getSelectedIndex() < 0) {
                if (validos) {
                    invalidos += ((JFXComboBox) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXComboBox) node).getPromptText();
                }
                validos = false;
            }
        }
        if (validos) {
            return "";
        } else {
            return "Campos requeridos o con problemas de formato (" + invalidos + ").";
        }
    }

    private void bindEmpleado(Boolean nuevo) {
        if (!nuevo) {
            txtId.textProperty().bind(empleado.empId);//bind de una sola direccion
        }
        txtNombre.textProperty().bindBidirectional(empleado.empNombre); //bind de ambas direcciones
        txtPApellido.textProperty().bindBidirectional(empleado.empPapellido);
        txtSApellido.textProperty().bindBidirectional(empleado.empSapellido);
        txtCedula.textProperty().bindBidirectional(empleado.empCedula);
        txtCorreo.textProperty().bindBidirectional(empleado.empCorreo);
        txtUsuario.textProperty().bindBidirectional(empleado.empUsuario);
        txtContrasena.textProperty().bindBidirectional(empleado.empClave);
        dtpFIngreso.valueProperty().bindBidirectional(empleado.empFingreso);
        dtpFSalida.valueProperty().bindBidirectional(empleado.empFsalida);
        chkAdministrador.selectedProperty().bindBidirectional(empleado.empAdministrador);
        chkActivo.selectedProperty().bindBidirectional(empleado.empEstado);
        BindingUtils.bindToggleGroupToProperty(tggGenero, empleado.empGenero);
    }

    private void unbindEmpleado() {
        txtId.textProperty().unbind();
        txtNombre.textProperty().unbindBidirectional(empleado.empNombre);
        txtPApellido.textProperty().unbindBidirectional(empleado.empPapellido);
        txtSApellido.textProperty().unbindBidirectional(empleado.empSapellido);
        txtCedula.textProperty().unbindBidirectional(empleado.empCedula);
        txtCorreo.textProperty().unbindBidirectional(empleado.empCorreo);
        txtUsuario.textProperty().unbindBidirectional(empleado.empUsuario);
        txtContrasena.textProperty().unbindBidirectional(empleado.empClave);
        dtpFIngreso.valueProperty().unbindBidirectional(empleado.empFingreso);
        dtpFSalida.valueProperty().unbindBidirectional(empleado.empFsalida);
        chkAdministrador.selectedProperty().unbindBidirectional(empleado.empAdministrador);
        chkActivo.selectedProperty().unbindBidirectional(empleado.empEstado);
        BindingUtils.unbindToggleGroupToProperty(tggGenero, empleado.empGenero);
    }

    private void nuevoEmpleado() {
        unbindEmpleado(); //Hacer el unbind para limpiar la interfas para poder bind correctamente 
        empleado = new EmpleadoDto();
        bindEmpleado(true);
        validarAdministrador();
        txtId.clear();
        txtId.requestFocus();
    }

    private void validarAdministrador() {
        if (chkAdministrador.isSelected()) {
            requeridos.addAll(Arrays.asList(txtCorreo,txtUsuario, txtContrasena));
            txtCorreo.setDisable(false);
            txtUsuario.setDisable(false);
            txtContrasena.setDisable(false);
        } else {
            requeridos.removeAll(Arrays.asList(txtCorreo,txtUsuario, txtContrasena));
            txtCorreo.validate();
            txtUsuario.validate();
            txtContrasena.validate();
            txtCorreo.clear();
            txtCorreo.setDisable(true);
            txtUsuario.clear();
            txtUsuario.setDisable(true);
            txtContrasena.clear();
            txtContrasena.setDisable(true);
        }
    }

    @FXML
    private void onKeyPressedTxtId(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER && !txtId.getText().isEmpty()) {
            cargarEmpleado(Long.valueOf(txtId.getText()));
        }
    }

    @FXML
    private void onActionChkAdministrador(ActionEvent event) {
        validarAdministrador();
    }

    @FXML
    private void onActionBtnNuevo(ActionEvent event) {
        if (new Mensaje().showConfirmation("Limpiar Empleado", getStage(), "Esta seguro que desea limpiar el registro?")) {
            nuevoEmpleado();
        }
    }

    @FXML
    private void onActionBtnBuscar(ActionEvent event) {
    }

    @FXML
    private void onActionBtnEliminar(ActionEvent event) {
        try {
            if (empleado.getEmpId() == null) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Eliminar empleado", getStage(), "Dede cargar el empleado que desea eliminar");
            } else {
                EmpleadoService service = new EmpleadoService();
                Respuesta respuesta = service.eliminarEmpleado(empleado.getEmpId());
                if (!respuesta.getEstado()) {
                    new Mensaje().showModal(Alert.AlertType.ERROR, "Eliminar empleado", getStage(), respuesta.getMensaje());
                } else {
                    new Mensaje().showModal(Alert.AlertType.INFORMATION, "Eliminar empleado", getStage(), "Empleado eliminado correctamente");
                    nuevoEmpleado();
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(EmpleadoViewController.class.getName()).log(Level.SEVERE, "Error eliminando el empleado.", ex);
            new Mensaje().showModal(Alert.AlertType.ERROR, "Eliminar empleado", getStage(), "Ocurrio un error eliminando el empleado");
        }
    }

    @FXML
    private void onActionBtnGuardar(ActionEvent event) {
        try {
            String invalidos = validarRequeridos();
            if (!invalidos.isEmpty()) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar empleado", getStage(), invalidos);
            } else {
                EmpleadoService service = new EmpleadoService();
                Respuesta respuesta = service.guardarEmpleado(empleado);
                if (!respuesta.getEstado()) {
                    new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar empleado", getStage(), respuesta.getMensaje());
                } else {
                    unbindEmpleado();
                    empleado = (EmpleadoDto) respuesta.getResultado("Empleado");
                    bindEmpleado(false);
                    new Mensaje().showModal(Alert.AlertType.INFORMATION, "Guardar empleado", getStage(), "Empleado guardado correctamente");
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(EmpleadoViewController.class.getName()).log(Level.SEVERE, "Error guardando el empleado.", ex);
            new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar empleado", getStage(), "Ocurrio un error guardando el empleado");
        }
    }

    @Override
    public void initialize() {

    }

    private void cargarEmpleado(Long id) {
        EmpleadoService service = new EmpleadoService();
        Respuesta respuesta = service.getEmpleado(id);

        if (respuesta.getEstado()) {
            unbindEmpleado();
            empleado = (EmpleadoDto) respuesta.getResultado("Empleado");
            bindEmpleado(false);
            validarAdministrador();
            validarRequeridos();
        } else {
            new Mensaje().showModal(Alert.AlertType.ERROR, "Cargar empleado", getStage(), respuesta.getMensaje());
        }

    }
}
