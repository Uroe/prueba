/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.unaplanilla.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTextField;
import cr.ac.una.unaplanilla.model.BancosDto;
import cr.ac.una.unaplanilla.model.TipoPlanillasDto;
import cr.ac.una.unaplanilla.service.BancosService;
import cr.ac.una.unaplanilla.service.TipoPlanillasService;
import cr.ac.una.unaplanilla.util.BindingUtils;
import cr.ac.una.unaplanilla.util.Formato;
import cr.ac.una.unaplanilla.util.Mensaje;
import cr.ac.una.unaplanilla.util.Respuesta;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Window;

/**
 * FXML Controller class
 *
 * @author Navarro
 */
public class BancosViewController extends Controller implements Initializable {

    @FXML
    private AnchorPane root;
    @FXML
    private JFXTextField txtId;
    @FXML
    private JFXCheckBox chkActivo;
    @FXML
    private JFXTextField txtNombre;
    @FXML
    private JFXRadioButton rdbEmpleado;
    @FXML
    private ToggleGroup tggAComicion;
    @FXML
    private JFXRadioButton rdbEmpresa;
    @FXML
    private JFXTextField txtComicionTransferencia;
    @FXML
    private JFXButton btnNuevo;
    @FXML
    private JFXButton btnBuscar;
    @FXML
    private JFXButton btnEliminar;
    @FXML
    private JFXButton btnGuardar;

    BancosDto banco;
    List<Node> requeridos = new ArrayList<>();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        txtNombre.setTextFormatter(Formato.getInstance().letrasFormat(30));
        rdbEmpleado.setUserData("E");
        rdbEmpresa.setUserData("T");
        txtComicionTransferencia.setTextFormatter(Formato.getInstance().cedulaFormat(5));
        banco = new BancosDto();
        nuevoBanco();
        indicarRequeridos();
    }

    private void bindBanco(Boolean nuevo) {
        if (!nuevo) {
            txtId.textProperty().bind(banco.id);
        }
        chkActivo.selectedProperty().bindBidirectional(banco.estado);
        txtNombre.textProperty().bindBidirectional(banco.nombre);
        BindingUtils.bindToggleGroupToProperty(tggAComicion, banco.Acomicion);
        txtComicionTransferencia.textProperty().bindBidirectional(banco.comicionTransferencia);
    }

    private void unbindBanco() {
        txtId.textProperty().unbind();
        chkActivo.selectedProperty().unbindBidirectional(banco.estado);
        txtNombre.textProperty().unbindBidirectional(banco.nombre);
        BindingUtils.unbindToggleGroupToProperty(tggAComicion, banco.Acomicion);
        txtComicionTransferencia.textProperty().unbindBidirectional(banco.comicionTransferencia);
    }

    private void nuevoBanco() {
        unbindBanco();
        banco = new BancosDto();
        bindBanco(true);
        txtId.clear();
        txtId.requestFocus();
    }

    public void indicarRequeridos() {
        requeridos.clear();
        requeridos.addAll(Arrays.asList(txtNombre, txtComicionTransferencia));
    }

    public String validarRequeridos() {
        Boolean validos = true;
        String invalidos = " ";
        for (Node node : requeridos) {
            if (node instanceof JFXTextField && ((JFXTextField) node).getText() == null) {
                if (validos) {
                    invalidos += ((JFXTextField) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXTextField) node).getPromptText();
                }
                validos = false;
            } else if (node instanceof JFXPasswordField && ((JFXPasswordField) node).getText() == null) {
                if (validos) {
                    invalidos += ((JFXPasswordField) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXPasswordField) node).getPromptText();
                }
                validos = false;
            } else if (node instanceof JFXDatePicker && ((JFXDatePicker) node).getValue() == null) {
                if (validos) {
                    invalidos += ((JFXDatePicker) node).getAccessibleText();
                } else {
                    invalidos += "," + ((JFXDatePicker) node).getAccessibleText();
                }
                validos = false;
            } else if (node instanceof JFXComboBox && ((JFXComboBox) node).getSelectionModel().getSelectedIndex() < 0) {
                if (validos) {
                    invalidos += ((JFXComboBox) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXComboBox) node).getPromptText();
                }
                validos = false;
            }
        }
        if (validos) {
            return "";
        } else {
            return "Campos requeridos o con problemas de formato (" + invalidos + ").";
        }
    }

    @FXML
    private void onKeyPressedTxtId(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER && !txtId.getText().isEmpty()) {
            cargarBanco(Long.valueOf(txtId.getText()));
        }
    }

    @FXML
    private void onActionBtnNuevo(ActionEvent event) {
        if (new Mensaje().showConfirmation("Limpiar Banco", getStage(), "Esta seguro que desea limpiar el registro?")) {
            nuevoBanco();
        }
    }

    @FXML
    private void onActionBtnBuscar(ActionEvent event) {
    }

    @FXML
    private void onActionBtnEliminar(ActionEvent event) {
        try {
            if (banco.getId() == null) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Eliminar banco", getStage(), "Dede cargar el banco que desea eliminar");
            } else {
                BancosService service = new BancosService();
                Respuesta respuesta = service.eliminarBanco(banco.getId());
                if (!respuesta.getEstado()) {
                    new Mensaje().showModal(Alert.AlertType.ERROR, "Eliminar banco", getStage(), respuesta.getMensaje());
                } else {
                    new Mensaje().showModal(Alert.AlertType.INFORMATION, "Eliminar Banco", getStage(), "Banco eliminado correctamente");
                    nuevoBanco();
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(BancosViewController.class.getName()).log(Level.SEVERE, "Error eliminando el banco.", ex);
            new Mensaje().showModal(Alert.AlertType.ERROR, "Eliminar banco", getStage(), "Ocurrio un error eliminando el banco");
        }
    }

    @FXML
    private void onActionBtnGuardar(ActionEvent event) {
        try {
            String invalidos = validarRequeridos();
            if (!invalidos.isEmpty()) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar banco", getStage(), invalidos);
            } else {
                BancosService service = new BancosService();
                Respuesta respuesta = service.guardarBanco(banco);
                if (!respuesta.getEstado()) {
                    new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar banco", getStage(), respuesta.getMensaje());
                } else {
                    unbindBanco();
                    banco = (BancosDto) respuesta.getResultado("Banco");
                    bindBanco(false);
                    new Mensaje().showModal(Alert.AlertType.INFORMATION, "Guardar banco", getStage(), "Banco guardado correctamente");
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(BancosViewController.class.getName()).log(Level.SEVERE, "Error guardando el banco.", ex);
            new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar planilla", getStage(), "Ocurrio un error guardando el banco");
        }
    }

    private void cargarBanco(Long id) {
        BancosService service = new BancosService();
        Respuesta respuesta = service.getBanco(id);

        if (respuesta.getEstado()) {
            unbindBanco();
            banco = (BancosDto) respuesta.getResultado("Banco");
            bindBanco(false);
            validarRequeridos();
        } else {
            new Mensaje().showModal(Alert.AlertType.ERROR, "Cargar Banco", getStage(), respuesta.getMensaje());
        }
    }

    @Override
    public void initialize() {
    }
}
