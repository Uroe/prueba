/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.unaplanilla.service;


import cr.ac.una.unaplanilla.model.TipoPlanillasDto;
import cr.ac.una.unaplanilla.util.Respuesta;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Navarro
 */
public class TipoPlanillasService {
    
    public Respuesta getPlanilla(Long id) {
        try {
            TipoPlanillasDto planilla = new TipoPlanillasDto();
            planilla.setId(Long.valueOf("1")); 
            planilla.setCodigo("12345");
            return new Respuesta(true, "", "", "Planilla", planilla);
        } catch (Exception ex) {
            Logger.getLogger(TipoPlanillasService.class.getName()).log(Level.SEVERE, "Error obteniendo la planilla [" + id + "]", ex);
            return new Respuesta(false, "Error obteniendo la planilla.", "getPlanilla" + ex.getMessage());
        }
    }
    public Respuesta getEmpleado(Long id) {
        try {
            TipoPlanillasDto planilla = new TipoPlanillasDto();
            planilla.setIdEmpleado(Long.valueOf("1")); 
            planilla.setNombre("Josue");
            return new Respuesta(true, "", "", "Empleado", planilla);
        } catch (Exception ex) {
            Logger.getLogger(TipoPlanillasService.class.getName()).log(Level.SEVERE, "Error obteniendo el empleado [" + id + "]", ex);
            return new Respuesta(false, "Error obteniendo el Empleado.", "getEmpleado" + ex.getMessage());
        }
    }
    
    public Respuesta guardarPlanilla(TipoPlanillasDto planilla) {
        try {
            planilla.setId(1L);
            return new Respuesta(true, "", "", "Planilla", planilla);
        } catch (Exception ex) {
            Logger.getLogger(TipoPlanillasService.class.getName()).log(Level.SEVERE, "Error guardando la planilla.", ex);
            return new Respuesta(false, "Error guardando la planilla.", "guardarPlanilla " + ex.getMessage());
        }
    }
    
    public Respuesta eliminarPlanilla(Long id) {
        try {
            return new Respuesta(true, "", "");
        } catch (Exception ex) {
            Logger.getLogger(TipoPlanillasService.class.getName()).log(Level.SEVERE, "Error eliminando la planilla.", ex);
            return new Respuesta(false, "Error eliminando la planilla.", "eliminarPlanilla " + ex.getMessage());
        }
    }
}
