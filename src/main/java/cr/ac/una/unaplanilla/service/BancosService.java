/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.unaplanilla.service;

import cr.ac.una.unaplanilla.model.BancosDto;
import cr.ac.una.unaplanilla.util.Respuesta;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Navarro
 */
public class BancosService {

    public Respuesta getBanco(Long id) {
        try {
            BancosDto banco = new BancosDto();
            banco.setId(Long.valueOf("1"));
            banco.setNombre("Bcr");
            return new Respuesta(true, "", "", "Banco", banco);
        } catch (Exception ex) {
            Logger.getLogger(BancosService.class.getName()).log(Level.SEVERE, "Error obteniendo el banco [" + id + "]", ex);
            return new Respuesta(false, "Error obteniendo el banco.", "getBanco" + ex.getMessage());
        }
    }

    public Respuesta guardarBanco(BancosDto banco) {
        try {
            banco.setId(1L);
            return new Respuesta(true, "", "", "Banco", banco);
        } catch (Exception ex) {
            Logger.getLogger(BancosService.class.getName()).log(Level.SEVERE, "Error guardando el banco.", ex);
            return new Respuesta(false, "Error guardando el banco.", "guardarBanco " + ex.getMessage());
        }
    }

    public Respuesta eliminarBanco(Long id) {
        try {
            return new Respuesta(true, "", "");
        } catch (Exception ex) {
            Logger.getLogger(BancosService.class.getName()).log(Level.SEVERE, "Error eliminando el banco.", ex);
            return new Respuesta(false, "Error eliminando el banco.", "eliminarBanco " + ex.getMessage());
        }
    }
}
